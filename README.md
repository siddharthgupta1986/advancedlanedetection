#Advanced Lane Finding Project

## Steps

* Camera calibration computation using a set of chessboard images
* Compute perspective and inverse perspective transform
* Define image thresholding pipeline
* Define methods for curvature, distance from center and outlier removal methods
* Define pipelines for lane search from scratch and lane search if previous frame had a robust search succeed
* Execute the piplelines for videos


[//]: # (Image References)

[image1]: ./output_images/undistort_calibration.png "Undistorted Calibration"
[image2]: ./output_images/undistort_output.png "Undistorted Test Image" 
[image3]: ./output_images/perspective_shift.png "Perspective Shift"
[image4]: ./output_images/thresholding.png "Thresholding Example"
[image5]: ./output_images/fit_lines.png "Fit Visual"
[image6]: ./output_images/image_plot.png "Plotted Result"

---
###Writeup / README

####1. Briefly state how you computed the camera matrix and distortion coefficients. Provide an example of a distortion corrected calibration image.

I take as input calibration images that are provided to us in the camera_cal folder. These images are taken of a chessboard of known dimensions. I compute object points and image points using cv2 functions. Beyond which I will use the img_points and obj_points to compute the camera calibration matrix.

![alt text][image1]

####2. Provide an example of a distortion-corrected image.
To demonstrate this step, I will describe how I apply the distortion correction to one of the test images like this one:
![alt text][image2]

####3. Describe how (and identify where in your code) you performed a perspective transform and provide an example of a transformed image.
I did the transformation step first so that I can focus my attention of thresholding only on the area that really matters for lane detection. In the python notebook, I have defined the translation points that are used to generate a 'bird's eye' view of the road. The points are used for generating both perspective shift and inverse perspective shift images. In the pipleline the perspective shift will be applied once the images are calibrated.

Code for perspective shift is as follows:
```
a = (200, 720)
b = (1125, 720)
c = (700, 450)
d = (600, 450)

ap = (200, 720)
bp = (1000, 720)
cp = (1000, 0)
dp = (200, 0)


src_vertices = np.float32([[a[0],a[1]], [b[0], b[1]], [c[0], c[1]], [d[0], d[1]]])
dest_vertices = np.float32([[ap[0], ap[1]], [bp[0], bp[1]], [cp[0], cp[1]], [dp[0], dp[1]]])

perspective_mat = cv2.getPerspectiveTransform(src_vertices, dest_vertices)
inv_perspective_mat = cv2.getPerspectiveTransform(dest_vertices, src_vertices)

def get_prespective_transformed_image(img):
    img_size = (img.shape[1], img.shape[0])
    return cv2.warpPerspective(img, perspective_mat, img_size, flags=cv2.INTER_LINEAR)
```  

I verified that my perspective transform was working as expected by drawing the `src` and `dst` points onto a test image and its perspective transformed counterpart to verify that the lines appear parallel in the perspective transformed image.
Example of perspective shift:

![alt text][image3]


####4. Describe how (and identify where in your code) you used color transforms, gradients or other methods to create a thresholded binary image.  Provide an example of a binary image result.
I did a lot of work on this portion of the code. I tried gradient based thresholding and color based thresholding and realized the best solution for the input video is using the color based threshold. In the code I used thresholding in RGB, HSV and HLS color spaces. Thresholding is applied to the perspective transformed images. The results came out to be really good using these combinations of filters.

Resulting Imagee:
![alt text][image4]

Code:
```
def hls_s_threshold(hls_img, thresh=(100,255)):
    hls_s = hls_img[:,:,2]
    binary = np.zeros_like(hls_s)
    binary[(hls_s > thresh[0]) & (hls_s <= thresh[1])] = 1
    return binary

def hsv_yellow_threshold(hsv_img, low=(15, 100, 100), high=(35, 255, 255)):
    return cv2.inRange(hsv_img, low, high)

def hsv_white_threshold(hsv_img, low=(0, 0, 250), high=(180, 255, 255)):
    return cv2.inRange(hsv_img, low, high)

def rgb_white_threshold(rgb_img, low=(210,210,210), high=(255, 255, 255)):
    return cv2.inRange(rgb_img, low, high)

def rgb_red_threshold(rgb_img, thresh=(220, 255)):
    rgb_red = rgb_img[:,:,0]
    binary = np.zeros_like(rgb_red)
    binary[(rgb_red > thresh[0]) & (rgb_red <= thresh[1])] = 1
    return binary

def rgb_green_threshold(rgb_img, thresh=(200, 255)):
    rgb_green = rgb_img[:,:,1]
    binary = np.zeros_like(rgb_green)
    binary[(rgb_green > thresh[0]) & (rgb_green <= thresh[1])] = 1
    return binary
```
```
def thresholding_pipeline(rgb_img):
    rgb_white = rgb_white_threshold(rgb_img)
    rgb_r_thresh = rgb_red_threshold(rgb_img)
    rgb_g_thresh = rgb_green_threshold(rgb_img)
    
    hsv_img = cv2.cvtColor(rgb_img, cv2.COLOR_RGB2HSV)
    hsv_yellow_thresh = hsv_yellow_threshold(hsv_img)
    hsv_white_thresh = hsv_white_threshold(hsv_img)
    
    hls_img = cv2.cvtColor(rgb_img, cv2.COLOR_RGB2HLS)
    hls_s_thresh = hls_s_threshold(hls_img)
    
    return rgb_white | (rgb_r_thresh & rgb_g_thresh) | hsv_yellow_thresh | hsv_white_thresh | hsv_white_thresh
```


####5. Describe how (and identify where in your code) you identified lane-line pixels and fit their positions with a polynomial?
First step in lane detection is to detect, in lower half of the image, begining of the lane lines. This gives our sliding window protocol a reference point to start from.
In next step I use sliding window protocol to search for line pixels while traversing up the y axis. Everytime, I only look for pixels in a margin left and right of the last sliding window. This ensures that I am not getting lost in any no lane line pixels on the road.
I used a degree two polynomial to fit in the identified lane line pixels from the thresholded image. 

![alt text][image5]

####6. Describe how (and identify where in your code) you calculated the radius of curvature of the lane and the position of the vehicle with respect to center.

To compute radius of curvature in meters, I used the following method:

```
def get_curvature_rad_in_m(x, y):
    ploty = np.linspace(0, 719, num=720)# to cover same y-range as image
    y_eval = np.max(ploty)
    
    # Define conversions in x and y from pixels space to meters
    ym_per_pix = 30/720 # meters per pixel in y dimension
    xm_per_pix = 3.7/700 # meters per pixel in x dimension

    fit_cr = np.polyfit(y*ym_per_pix, x*xm_per_pix, 2)
    curverad = ((1 + (2*fit_cr[0]*y_eval*ym_per_pix + fit_cr[1])**2)**1.5) / np.absolute(2*fit_cr[0])
    
    return curverad
```

####7. Provide an example image of your result plotted back down onto the road such that the lane area is identified clearly.

The plot of polynomial that I believe is the lane space of my current lane, is give shown in the example below:

![alt text][image6]

---

### Lane Detection Pipeline
#### Multi step process:
* Undistort image using camera calibration matrix
* Perspective transform calibrated images
* Get thresholded image using various color based thresholding
* In the lower half of the image, use histogram to find relevant section of the image where we need to search for lanes. Image search begins from bottom of the frame and moves to top of the frame
* Use sliding window technique to look for lane pixels for the full height of the image
* Use the detected pixels to generate a fit
* An optimization can be done wherein if we have found out the relevant fit in the previous image, we can use that data to optimize the search area in the following image.
* Using the fits that are generated we can compute the radius of curvature of the road and the distance from the center of the lane for the car.

---

###Pipeline (video)

####1. Provide a link to your final video output.  Your pipeline should perform reasonably well on the entire project video (wobbly lines are ok but no catastrophic failures that would cause the car to drive off the road!).

Here's a [link to my video result](./result.mp4)

---

###Discussion

####1. Briefly discuss any problems / issues you faced in your implementation of this project.  Where will your pipeline likely fail?  What could you do to make it more robust?

* Sobel operators did not perform well with various road conditions such as shadow and tar and cement road in same frame.
* If we have understanding of steering angles of the car we can co-relate that information while generating a lane mask for doing lane detection work. This will help remove some outliers.
* Debris on the side of the road can sometime impact the calculation of fit lines. To overcome that issue, I used gaussian elimination of pixel on x axis for both left and right lines.
* Shadows or sunlight directly pointing into the camera can easily cause failures in the models that I have created. To some extent, we can solve this by controling the camera exposure in realtime, but to solve this problem more effectively we might have to look beyond camera as the only sensor.


####2. Future Scope (a.k.a. making pipeline more robust)
* I can implement logic to test whether the detected lines are indeed lanes.
* I can implement a mask to remove from consideration a part of road which we know should not contain any road markings.
* We can use gaussian blur to help get rid of any small debris that can affect lane detection.
* We can correlate the lane curvature data with realtime gps and open map data to confirm if the detected radius does indeed match the expected values.
* We can look at sensor beyond visibile light camera sensors to improve the model performance in changing light conditions